package com.itau.comunicador.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

@Service
public class EmailService {

	@Autowired
	private AmazonSimpleEmailService sesService;

	public void sendEmail(String message, String subject, String email) {

		try {

			SendEmailRequest request = new SendEmailRequest().withDestination(new Destination().withToAddresses(email))
					.withMessage(new Message()
							.withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(message))
									.withText(new Content().withCharset("UTF-8").withData("Comunicado Itaú")))
							.withSubject(new Content().withCharset("UTF-8").withData(subject)))
					.withSource("pedrohenriquetpr@gmail.com");

			sesService.sendEmail(request);
			
			System.out.println("Email enviado");

		} catch (Exception e) {
			throw e;
		}
	}
}

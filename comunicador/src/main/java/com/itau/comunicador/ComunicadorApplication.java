package com.itau.comunicador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ComunicadorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComunicadorApplication.class, args);
	}

}

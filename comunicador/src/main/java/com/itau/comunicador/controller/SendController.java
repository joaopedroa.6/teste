package com.itau.comunicador.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.itau.comunicador.api.SendApi;
import com.itau.comunicador.api.model.EmailRequest;
import com.itau.comunicador.api.model.SmsRequest;
import com.itau.comunicador.service.EmailService;
import com.itau.comunicador.service.SmsService;

@RestController
public class SendController implements SendApi {

	@Autowired
	private SmsService smsService;

	@Autowired
	private EmailService emailService;

	@Override
	public ResponseEntity<Void> sendSMS(SmsRequest smsRequest) {
		smsService.sendSingleSMS(smsRequest.getMessage(), smsRequest.getPhoneNumber());
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> sendEmail(EmailRequest emailRequest) {

		emailService.sendEmail(emailRequest.getTemplate(), emailRequest.getSubject(), emailRequest.getEmail());
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}

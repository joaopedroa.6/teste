package com.itau.detran.configuration;

import java.net.http.HttpClient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HttpClientConfiguration {

	@Bean
	public HttpClient getHttpClient() {

		return HttpClient.newHttpClient();

	}

}

package com.itau.detran.configuration;

import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.TimeoutException;

import org.springframework.context.annotation.Configuration;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;

@Configuration
public class RetryConfiguration {

	public Retry getRetry(Integer maxAttemps, Duration duration, Integer statusCode, String name) {

		RetryConfig config = RetryConfig.<HttpResponse<Object>>custom()
				.maxAttempts(maxAttemps)
				.waitDuration(duration)
				.retryExceptions(TimeoutException.class, Exception.class, IllegalArgumentException.class)
				.retryOnResult(response -> response.statusCode() != statusCode)
				.build();

		RetryRegistry registry = RetryRegistry.of(config);

		return registry.retry(name, config);

	}

}

package com.itau.detran.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.detran.api.model.TributoModel;
import com.itau.detran.configuration.RetryConfiguration;

import io.github.resilience4j.retry.Retry;

@Service
public class ProdespService {
	
	@Autowired
	private RetryConfiguration retryConfig;

	@Autowired
	private HttpClient httpClient;

	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private PagamentoService pagamentoService;
	
	public List<TributoModel> getTributosPendentesPagamento(String cnpj, String tipoTributo){
		
		try {
			
			Retry retry = retryConfig.getRetry(3, Duration.ofSeconds(3), HttpStatus.OK.value(),
					this.getClass().getName());

			var request = HttpRequest.newBuilder()
					.uri(URI.create("https://my-json-server.typicode.com/pedrohenriquea/prodesp-fake-api/tributos"))
					.header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
					.header("cpf-cnpj", cnpj)
					.header("tipo-tributo", tipoTributo)
					.method(HttpMethod.GET.toString(), BodyPublishers.ofString(""))
					.build();

			var retryCall = Retry.decorateCheckedSupplier(retry, () -> httpClient.send(request, BodyHandlers.ofString()));

			var response = retryCall.apply();

			if (response.statusCode() == HttpStatus.OK.value())
				return mapper.readValue(response.body(), new TypeReference<List<TributoModel>>() {});
			else
				throw new Exception(String.format("Status Code: %s | Body: %s", response.statusCode(), response.body()));
			
		} catch (Throwable e) {
			throw new RuntimeException("");
		}
	}
	
	public List<TributoModel> getTributosPagos(String cpf) {

		try {

			Retry retry = retryConfig.getRetry(3, Duration.ofSeconds(3), HttpStatus.OK.value(),
					this.getClass().getName());

			var request = HttpRequest.newBuilder()
					.uri(URI.create("https://my-json-server.typicode.com/pedrohenriquea/prodesp-fake-api/tributos"))
					.header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
					.header("cpf-cnpj", cpf)
					.header("status", "Pago").method(HttpMethod.GET.toString(), BodyPublishers.ofString(""))
					.build();

			var retryCall = Retry.decorateCheckedSupplier(retry, () -> httpClient.send(request, BodyHandlers.ofString()));

			var response = retryCall.apply();

			if (response.statusCode() == HttpStatus.OK.value())
				return mapper.readValue(response.body(), new TypeReference<List<TributoModel>>() {
				});
			else
				throw new Exception(String.format("Status Code: %s | Body: %s", response.statusCode(), response.body()));

		} catch (Throwable e) {
			throw new RuntimeException("");
		}
	}
	
	public void pagarTributosSelecionados(List<TributoModel> tributos, String cpf) {
		
		try {
			
			Double valorTotalPagamento = tributos.stream().map(TributoModel::getValor).reduce(0D, Double::sum);
			
			pagamentoService.realizarPagamento(cpf, valorTotalPagamento);
			
			// enviar sms
			// enviar email comprovante
		} catch (Exception e) {
			// Enviar sms erro
			throw new RuntimeException("");
		}
	}
	
	@SuppressWarnings("unused")
	private void solicitarBaixaDePagamento(List<TributoModel> tributos, String cpf) {

		try {

			var request = HttpRequest.newBuilder()
					.uri(URI.create("https://my-json-server.typicode.com/pedrohenriquea/prodesp-fake-api/pagamentos"))
					.header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
					.method(HttpMethod.POST.toString(), 
							BodyPublishers.ofString(mapper.writeValueAsString(tributos)))
					.build();

			var response = httpClient.send(request, BodyHandlers.ofString());

			if (response.statusCode() != HttpStatus.OK.value()) {
				throw new Exception("");
			}

		} catch (Exception e) {
			pagamentoService.realizarEstornoPagamento(cpf, tributos.stream().map(TributoModel::getValor).reduce(0D, Double::sum));
		}
	}

}

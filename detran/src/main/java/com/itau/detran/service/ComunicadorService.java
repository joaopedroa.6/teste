package com.itau.detran.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.detran.feigns.ComunicadorFeign;
import com.itau.detran.model.comunicador.EmailRequest;
import com.itau.detran.model.comunicador.SmsRequest;

@Service
public class ComunicadorService {
	
	@Autowired
	private ComunicadorFeign comunicadorFeign;
	
	
	public void enviarEmail(EmailRequest emailRequest) {
		comunicadorFeign.sendEmail(emailRequest);
	}
	
	public void enviarSms(SmsRequest smsRequest) {
		comunicadorFeign.sendSMS(smsRequest);
	}
}

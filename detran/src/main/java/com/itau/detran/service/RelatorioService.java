package com.itau.detran.service;

import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.itau.detran.api.model.TributoModel;
import com.itau.detran.model.comunicador.EmailRequest;
import com.itau.detran.model.comunicador.SmsRequest;
import com.itau.detran.service.amazon.AWSS3Service;

@Service
public class RelatorioService {

	@Value("${cloud.aws.end-point.uri}")
	private String endPoint;

	@Autowired
	private ProdespService prodespService;

	@Autowired
	private QueueMessagingTemplate queueMessagingTemplate;
	
	@Autowired
	private AWSS3Service awss3Service;
	
	@Autowired
	private ComunicadorService comunicadorService;

	public void putMessageQueue(String message) {
		queueMessagingTemplate.convertAndSend(endPoint, MessageBuilder.withPayload(message).build());
	}

	public void gerarRelatorio(String cpf) {

		try {
			
			List<TributoModel> tributosPagos = prodespService.getTributosPagos(cpf);
			
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheetRelatorio = workbook.createSheet("Tributos Pagos");
			
			sheetRelatorio.setColumnWidth(0, 2000);
			sheetRelatorio.setColumnWidth(1, 2000);
			sheetRelatorio.setColumnWidth(2, 2000);
			sheetRelatorio.setColumnWidth(3, 2000);
			sheetRelatorio.setColumnWidth(4, 2000);
			
			int rowNum = 0;
			
			Row rowCabecalho = sheetRelatorio.createRow(rowNum++);
			
			Cell cellTituloCodigo = rowCabecalho.createCell(0);
			cellTituloCodigo.setCellValue("Codigo");
			Cell cellTituloCpf = rowCabecalho.createCell(1);
			cellTituloCpf.setCellValue("Cpf");
			Cell cellTituloDescricao = rowCabecalho.createCell(2);
			cellTituloDescricao.setCellValue("Descricao");
			Cell cellTituloValor = rowCabecalho.createCell(3);
			cellTituloValor.setCellValue("Valor");
			Cell cellTituloTipo = rowCabecalho.createCell(4);
			cellTituloTipo.setCellValue("Tipo");
			
			for(TributoModel tributo: tributosPagos) {
				Row row = sheetRelatorio.createRow(rowNum++);
				Cell cellCodigo = row.createCell(0);
				cellCodigo.setCellValue(tributo.getCodigo());
				Cell cellCpf = row.createCell(1);
				cellCpf.setCellValue(tributo.getCpfCnpj());
				Cell cellDescricao = row.createCell(2);
				cellDescricao.setCellValue(tributo.getDescricao());
				Cell cellValor = row.createCell(3);
				cellValor.setCellValue(tributo.getValor());
				Cell cellTipo = row.createCell(4);
				cellTipo.setCellValue(tributo.getTipo());
			}
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workbook.write(bos);
			
			awss3Service.uploadFile(String.format("TRIBUTOS_PAGOS_%s.xlsx", cpf), bos.toByteArray());
			
			comunicadorService.enviarEmail(new EmailRequest("pedrohenriquea.6@hotmail.com", "<h1>Relatorio disponível</h1>", "Relatorio gerado"));
			comunicadorService.enviarSms(new SmsRequest("Relatório disponível", "+5531971521250"));
			
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}

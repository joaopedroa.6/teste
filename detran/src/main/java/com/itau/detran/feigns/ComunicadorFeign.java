package com.itau.detran.feigns;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.itau.detran.model.comunicador.EmailRequest;
import com.itau.detran.model.comunicador.SmsRequest;

@Component
@FeignClient(name = "comunicador-service", path = "/send")
public interface ComunicadorFeign {

	@PostMapping("/sms")
	public ResponseEntity<Void> sendSMS(@Valid @RequestBody SmsRequest smsRequest);

	@PostMapping("/email")
	public ResponseEntity<Void> sendEmail(@Valid @RequestBody EmailRequest emailRequest);
	
}
